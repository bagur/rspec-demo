require './hero'

describe Hero do
  before do
    @hero = Hero.new 'mike'
  end

  it 'has capitalized name' do
    expect(@hero.name).to eq 'Mike' # hero.name == 'Mike'
  end

  it 'can power_up' do
    expect(@hero.power_up).to eq 110 # hero.power_up == 110
  end

  it 'can power_up' do
    expect(@hero.power_down).to eq 90 # hero.power_up == 90
  end

  it 'id displays hero info' do
    expect(@hero.info).to eq 'Mike[100]' # hero.info == 'Mike[100]'
  end
end
